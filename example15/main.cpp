#include <iostream>
#include <string>

// Dummy function which is supposed to log anything
template<typename T>
void log(T&& obj) noexcept
{
    // Any exceptionless logging
}

// Dummy class providing 2 constructors, one is noexcept
class Test
{
public:
    Test() noexcept {}
    Test(std::string name) {}
};

// Factory function that creates any object and logs it
template<typename T, typename ...Args>
T createAndLog(Args... args) noexcept(noexcept(T{args...}))
{
    T obj{args...};
    log(obj);

    return obj;
}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: noexcept operator" << endl;

    cout << "createAndLog<int>(5) " << (noexcept(createAndLog<int>(5)) ? "cannot throw" : "can throw") << endl;
    createAndLog<int>(5);           // noexcept

    cout << "createAndLog<Test>() " << (noexcept(createAndLog<Test>()) ? "cannot throw" : "can throw") << endl;
    createAndLog<Test>();           // noexcept

    cout << "createAndLog<Test>(\"value\") " << (noexcept(createAndLog<Test>("value")) ? "cannot throw" : "can throw") << endl;
    createAndLog<Test>("value");    // noexcept(false)
    
    return EXIT_SUCCESS;
}
