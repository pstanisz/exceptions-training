#include <iostream>
#include <string>
#include <exception>

// Connection class is throwing an exception from constructor
class Connection
{
public:
    Connection() : Connection("127.0.0.1")
    {
        std::cout << "Delegating ctor" << std::endl;

        // Exception thrown here would not prevent from calling destructor,
        // as the instance was already constructed
        throw std::runtime_error("runtime");
    }

    explicit Connection(const std::string& hostName)
    {
        std::cout << "Connection ctor: " << hostName << std::endl;

        // Exception thrown here would prevent from calling destructor,
        // as the instance is partially constructed
        //throw std::runtime_error("runtime");
    }

    ~Connection() noexcept(true)
    {
        std::cout << "Connection dtor" << std::endl;
    }
};

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: delegating constructor and destruction" << endl;

    try
    {
        // Ordinary constructor called
        Connection connection("hostName");

        // Delegating constructor called - destructor should be called
        // despite the fact that it throws an exception
        Connection delegatedConnection;
    }
    catch (std::exception& error)
    {
        // Handling standard exceptions
        cout << "Standard exception caught: " << error.what() << endl;
    }
    catch (...)
    {
        // Handling anything
        cout << "Non-standard exception caught" << endl;
    }
    
    return EXIT_SUCCESS;
}
