#include <iostream>
#include <string>
#include <exception>

enum class Failures
{
    OK,
    Lights,
    Engine,
    Battery,
    Any
};

class Failure : public std::exception
{
public:
    Failure(Failures failure = Failures::Any) : m_failureCode(failure) {}

    Failures m_failureCode;
};

class IgnitionFailure : public Failure {};
class ParkFailure : public Failure {};

// Dummy Car class which mixes an exceptions with error codes
class Car
{
public:
    void ignition() { throw IgnitionFailure(); }
    void drive() { throw Failure(Failures::Engine); }
    void park() { throw ParkFailure(); }
    Failure charge() { return Failures::Battery; }
    void flyToMars() { throw Failure(Failures::Any); }
};

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: mixing exceptions with error codes" << endl;
    
    try
    {
        Car myTesla;

        myTesla.charge();       // Error ignored :( I thought it throws
        myTesla.ignition();
        myTesla.drive();
        myTesla.park();
        myTesla.flyToMars();    // Mission in danger! 
    }
    catch (const IgnitionFailure& error)
    {
        cout << "Ignition failure" << endl;
    }
    catch (const ParkFailure& error)
    {
        cout << "Park failure" << endl;
    }
    catch (const Failure& error)
    {
        switch (error.m_failureCode)
        {
            case Failures::Lights: cout << "Lights failure" << endl; break;
            case Failures::Engine: cout << "Engine failure" << endl; break;
            case Failures::Battery: cout << "Battery failure" << endl; break;
            case Failures::Any: cout << "Any failure" << endl; break;
            default: cout << "OK" << endl; break;
        }
    }
    catch (...)
    {
        cout << "Anything caught" << endl;
    }
    
    return EXIT_SUCCESS;
}
