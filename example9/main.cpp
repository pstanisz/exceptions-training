#include <iostream>
#include <string>

#include "loader.h"

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: -fno-exceptions with throwing library" << endl;

    // Installed terminate handler should be executed when an exception
    // is thrown from within an external library being used
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    withexceptions::Loader loader("/dummy/path/to/file.png");

    // This method throws an exception - compiling current example
    // without support of exceptions does not prevent it from termination
    loader.validate();

    cout << "Execution will never get so far" << endl;
    
    return EXIT_SUCCESS;
}
