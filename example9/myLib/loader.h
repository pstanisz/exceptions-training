#pragma once

#include <string>

namespace withexceptions
{

// Loader component interface is used to simulate
// the exception thrown from external library
class Loader
{
public:
    explicit Loader(const std::string& filePath);

    // This method throws an exception
    void validate();
};

}