#include "loader.h"

#include <stdexcept>

namespace withexceptions
{

Loader::Loader(const std::string& /*filePath*/)
{
    // Does nothing
}

void Loader::validate()
{
    throw std::runtime_error("Thrown from within myLib");
}

}