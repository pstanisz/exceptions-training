cmake_minimum_required(VERSION 2.8)
 
# Setting target name
set(TARGET_NAME example9)

# Disable exceptions for this example
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-exceptions" )

# Include directories
include_directories(
    .
    myLib
)

# Add the executable
add_executable(
    ${TARGET_NAME}
    main.cpp
)

# Add the library
add_subdirectory(
    myLib
)

# Link to myLib, which is compiled with exceptions
target_link_libraries(
    ${TARGET_NAME}
    myLib
)