#include <iostream>
#include <string>
#include <exception>

namespace exceptionless
{

// Class which stores an image
class Image {
public:
    /*!
     * \brief Applies filtering on image
     * \return 0 in case of success, -1 in case of failure
     */
    int filter() { return -1; }
};

/*!
 * \brief Loads an image
 * \param image Output parameter to store an image
 * \param path Path to image file
 * \return 0 in case of success, -1 in case of failure
 */
int loadImage(Image& image, const std::string& path)
{
    return 0;
}

/*!
 * \brief Loads an image
 * \param path Path to image file
 * \return std::pair<Image, int> where
 *      first - loaded image
 *      second - 0 in case of success, -1 otherwise
 */
std::pair<Image, int> loadImage(const std::string& path)
{
    return std::make_pair<Image, bool>(Image{}, 0);
}

}


namespace withexceptions
{

// Dummy exception class indicating image loading failure
class ImageLoadingFailed : public std::exception
{

};

// Dummy exception class indicating image filtering failure
class ImageFilteringFailed : public std::exception
{

};

// Class which stores an image
class Image
{
public:
    /*!
     * \brief Applies filtering on image
     * \throws ImageFilteringFailed in case of failure
     */
    void filter() { throw ImageFilteringFailed(); }
};

/*!
 * \brief Loads an image
 * \param path Path to image file
 * \throws ImageLoadingFailed exception on case of failure
 * \return Loaded image
 */
Image loadImage(const std::string& path)
{
    return Image{};
}

}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: cleaner code and interfaces" << endl;

    {
        using namespace exceptionless;

        Image image;
        if (loadImage(image, "/path/to/image") == -1)
        {
            // Handle image loading error
            cout << "Handle image loading error" << endl;
        }
        else
        {
            if (image.filter() == -1)
            {
                // Handle image filtering error
                cout << "Handle image filtering error" << endl;
            }
            else
            {
                // Work on filtered image
            }
        }
    }

    {
        using namespace exceptionless;

        auto result = loadImage("/path/to/image");
        if (result.second == -1)
        {
            // Handle image loading error
            cout << "Handle image loading error" << endl;
        }
        else
        {
            auto& image = result.first;
            if (image.filter() == -1)
            {
                // Handle image filtering error
                cout << "Handle image filtering error" << endl;
            }
            else
            {
                // Work on filtered image
            }
        }
    }

    {
        using namespace withexceptions;

        try
        {
            auto image = loadImage("/path/to/image");
            image.filter();

            // Work on filtered image
        }
        catch(ImageLoadingFailed& error)
        {
            // Handle image loading error
            cout << "Handle image loading error: " << error.what() << endl;
        }
        catch(ImageFilteringFailed& error)
        {
            // Handle image filtering error
            cout << "Handle image filtering error: " << error.what() << endl;
        }
    }
    
    return EXIT_SUCCESS;
}
