#include <iostream>
#include <string>

// Base class of exception to be used
class Error : public std::exception
{
public:
    explicit Error(const std::string& message) : m_message(message)
    {}

    virtual ~Error() noexcept = default;

    virtual const char* what() const noexcept
    {
        return m_message.c_str();
    }

private:
    std::string m_message;
};

// Dummy field reader
template<typename T>
class FieldReader
{
public:
    static T read(const std::string& nodeName)
    {
        throw Error("Value: \"" + nodeName + "\"");  // Simulated throw
        return T{};
    }
};

// Dummy object reader
template<typename T>
class ObjectReader
{
public:
    static T read(const std::string& nodeName) 
    try
    {
        FieldReader<double>::read("thermal_efficiency");
        return T{};
    } 
    catch (...)
    {
        std::throw_with_nested(Error("Object: \"" + nodeName + "\""));
    }
};

using Object = int;

// Dummy JSON reader
class JsonReader
{
public:
    explicit JsonReader(const std::string& filePath) 
    try
    {
        read<Object>("car");    // Ignoring read object
    }
    catch (...)
    {
        std::throw_with_nested(Error("Reading file failed: \"" + filePath + "\""));
    }

private:
    template<typename T>
    T read(const std::string& nodeName)
    try
    {
        return ObjectReader<T>::read("engine"); // Dummy type
    }
    catch (...)
    {
        std::throw_with_nested(Error("Object: \"" + nodeName + "\""));
    }
};

void printErrors(const std::exception& error, int level = 0)
{
    std::cerr << std::string(level, ' ') << error.what() << std::endl;
    try
    {
        std::rethrow_if_nested(error);
    }
    catch (const std::exception& error)
    {
        printErrors(error, level + 1);
    }
}

int main()
try
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: nested_exception" << endl;

    JsonReader reader("/etc/model/config.json");

    return EXIT_SUCCESS;
}
catch (const Error& error)
{
    printErrors(error);

    return EXIT_FAILURE;
}
