#include <iostream>
#include <string>
#include <exception>

/*!
 * \brief Non-throwing function
 * \return 0 in case of success, -1 in case of failure
 */
int foo()
{
    return -1;
}

/*!
 * \brief Throwing function
 * \throws std::runtime_error in case of failure
 */
void bar()
{
    throw std::runtime_error("Error");
}


int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: error codes vs exceptions" << endl;
    
    foo();  // If an error is returned - execution continues
    bar();  // If an exception is thrown - execution is terminated

    // Perform actions depending on foo/bar result
    
    return EXIT_SUCCESS;
}
