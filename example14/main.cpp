#include <iostream>
#include <string>
#include <csetjmp>

static std::jmp_buf jumpBuffer;

class Car
{
public:
    Car() { std::cout << "Car's ctor" << std::endl; }
    ~Car() noexcept { std::cout << "Car's dtor" << std::endl; }
    bool drive() { return false; }
};

void foo()
{
    Car myTesla;    // Notice, that destructor won't be executed
    if (!myTesla.drive())   // Error when driving
    {
        longjmp(jumpBuffer, 1); // Load stored execution context
    }
}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: using setjmp and longjmp" << endl;

    // Saving current execution context, jumpBuffer can later be used
    // to restore the current execution context by longjmp function
    if (setjmp(jumpBuffer) == 0)
    {
        foo();  // Function does a longjmp in case of error
    }
    else
    {
        cout << "Handling an error" << endl;
    }
    
    return EXIT_SUCCESS;
}
