#include <iostream>
#include <string>
#include <exception>
#include <thread>
#include <chrono>
#include <memory>
#include <functional>

int main()
{
    using std::cout;
    using std::endl;
    using std::chrono::milliseconds;

    cout << "Exceptions-training: exception from std::thread uncaught" << endl;

    // Installing custom terminate handler to confirm termination
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    try
    {
        // Launching the thread in try block won't help,
        // as escaping an exception from thread causes termination
        std::thread worker([](){
            cout << "Thread routine started" << endl;

            std::this_thread::sleep_for(milliseconds(200));

            throw std::runtime_error("Exception from worker");
        });

        // RAII used to join worker when going out of scope
        // Not joining or detaching worker before going out of scope
        // would cause program termination as well
        std::unique_ptr<std::thread, std::function<void(std::thread*)>> threadJoiner(
            &worker, [](std::thread* thread) { if (thread->joinable()) thread->join(); }
        );

        cout << "Waiting for join..." << endl;
    }
    catch (const std::exception& error)
    {
        cout << "Handling error: " << error.what() << endl;
    }
    catch (...)
    {
        cout << "Handling any error" << endl;
    }
    
    return EXIT_SUCCESS;
}
