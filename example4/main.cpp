#include <iostream>
#include <string>

// C-style function, returns 0 if success, -1 otherwise
int connect(const char* hostName)
{
    return 0;
}

namespace exceptionless
{

// Represents the connection
class Connection
{
public:
    /*!
     * \brief Opens the connection to given host
     * \param hostName Host name
     */
    explicit Connection(const std::string& hostName)
    {
        // Opening the connection
        m_status = connect(hostName.c_str());
    }

    /*!
     * \brief Destructor
     */
    ~Connection()
    {
        if (m_status == 0)
        {
            // Closing the connection
        }
    }

    /*!
     * \brief Checks whether connection is established
     * \return true if connected; false otherwise
     */
    bool isConnected() const { return m_status == 0; }

private:
    int m_status;   // Stores connection status
};

}

namespace withexceptions
{

// Dummy exception class indicating connection failure
class ConnectionFailure : public std::exception
{
};

// Represents the connection
class Connection
{
public:
    /*!
     * \brief Opens the connection to given host
     * \param hostName Host name
     */
    explicit Connection(const std::string& hostName)
    {
        // Opening the connection
        if (connect(hostName.c_str()) == -1)
        {
            throw ConnectionFailure();
        }
    }

    /*!
     * \brief Destructor
     */
    ~Connection()
    {
        // Closing the connection
    }
};

}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: exceptions from constructors" << endl;
    
    {
        using namespace exceptionless;
        
        Connection connection("127.0.0.1");

        // Possible 'zombie' connection exists here

        // Connection must be checked before using it
        if (connection.isConnected())
        {
            // Use connection
            cout << "Working with connection" << endl;
        }
        else
        {
            // Handle connection failure
            cout << "Handling connection failure" << endl;
        }
    }

    {
        using namespace withexceptions;
        
        try
        {
            Connection connection("127.0.0.1");

            // Valid connection - it can be safely used
            cout << "Working with connection" << endl;
        }
        catch (ConnectionFailure& error)
        {
            // Handle connection failure
            cout << "Handling connection failure" << endl;
        }
    }
    
    return EXIT_SUCCESS;
}
