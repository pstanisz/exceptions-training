cmake_minimum_required(VERSION 2.8)
 
# Setting target name
set(TARGET_NAME example4)

# Include directories
include_directories(
    .
)

# Add the executable
add_executable(
    ${TARGET_NAME}
    main.cpp
)
