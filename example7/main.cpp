#include <iostream>
#include <string>
#include <exception>

// Base class for my custom exception type
class BaseExcept : public std::exception
{
public:
    virtual ~BaseExcept() noexcept = default;

    virtual void print() const noexcept {
        std::cout << "Handling ExceptionBase" << std::endl;
    }
};

// Class derived from my custom exception type
class DerivedExcept : public BaseExcept
{
public:
    virtual void print() const noexcept override {
        std::cout << "Handling DerivedExcept" << std::endl;
    }
};

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: catching by value, ptr, ref, const ref" << endl;

    // Catching by value 
    try
    {
        // 'Handling ExceptionBase' expected
        throw DerivedExcept();
    }
    catch (BaseExcept error)
    {
        // Polymorphism is not working for value!
        error.print();
    }

    // Catching by pointer
    try
    {
        // 'Handling DerivedExcept' expected
        throw new DerivedExcept;
    }
    catch (BaseExcept* error)
    {
        // Polymorphism is working for pointer
        error->print();

        // But should an error be deleted? Or not?
        delete error;
    }

    // Catching by reference
    try
    {
        // 'Handling DerivedExcept' expected
        throw DerivedExcept();
    }
    catch (BaseExcept& error)
    {
        // Polymorphism is working for reference
        error.print();
    }

    // Catching by const reference
    try
    {
        // 'Handling DerivedExcept' expected
        throw DerivedExcept();
    }
    catch (const BaseExcept& error)
    {
        // Polymorphism is working for const reference
        error.print();
    }
    
    return EXIT_SUCCESS;
}
