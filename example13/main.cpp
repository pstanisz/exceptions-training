#include <iostream>
#include <string>
#include <exception>
#include <chrono>
#include <future>

int main()
{
    using std::cout;
    using std::endl;
    using std::thread;
    using std::chrono::milliseconds;

    cout << "Exceptions-training: std::async and exceptions" << endl;

    // Installing custom terminate handler to confirm termination
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    try
    {
        // No need to catch an exception when using std::async!
        auto myFuture = std::async(std::launch::async, []() {
            cout << "Async task started" << endl;
            std::this_thread::sleep_for(milliseconds(400));

            throw std::runtime_error("Exception from task");
        });

        cout << "Waiting for future..." << endl;
        myFuture.wait();    // Waits until task execution finish
        myFuture.get();     // Throws an exception from task
    }
    catch (const std::exception& error)
    {
        cout << "Handling error: " << error.what() << endl;
    }
    catch (...)
    {
        cout << "Handling any error" << endl;
    }
    
    return EXIT_SUCCESS;
}
