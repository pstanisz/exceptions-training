#include <iostream>
#include <string>

/*!
 * \brief Dummy function
 */
void foo()
{
    std::cout << "fun() called" << std::endl;
}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: basic try/catch structure" << endl;
    
    try
    {
        // Possibly throwing sequence of statements
        // throw 1;
        // throw std::runtime_error("runtime");
        // throw 99.0f;
        // throw foo;      // function: void foo();
    }
    catch (int)  // Handler with unnamed parameter
    {
        // Handling an 'int' exception
        cout << "'int' caught" << endl;
    }
    catch (std::exception& ex)  // Handler with named parameter
    {
        // Handling an exception of std::exception or derived type
        cout << "std::exception caught: " << ex.what() << endl;
    }
    catch (void(*ptr)())    // Handler with named parameter
    {
        // Handling the function pointer exception
        cout << "Function pointer caught - calling it" << endl;
        ptr();
    }
    catch (...) // Catch-all handler
    {
        // Handling an exception of any type
        cout << "Anything caught" << endl;
    }
    
    return EXIT_SUCCESS;
}
