#include <iostream>
#include <string>
#include <exception>

// Function that should not throw any exception
// It uses noexcept specifier
void foo() noexcept
{
    throw std::runtime_error("runtime");
}

// Function that should not throw any exception
// It uses throw() specifier
void bar() throw()
{
    throw std::runtime_error("runtime");
}

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: noexcept vs. throw()" << endl;

    // Installed terminate handler should be executed when an exception
    // from destructor is thrown during stack unwinding
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    std::set_unexpected([](){
        cout << "Custom unexpected-handler called" << endl;
        throw std::bad_exception();
    });

    try
    {
        //foo();  // terminate at once
        bar();  // unexpecter handler, then terminate
    }
    catch (const std::exception& error)
    {
        cout << "Handling error: " << error.what() << endl;
    }
    catch (...)
    {
        cout << "Handling any error" << endl;
    }
    
    return EXIT_SUCCESS;
}
