#include <iostream>
#include <string>
#include <exception>
#include <thread>
#include <chrono>
#include <future>
#include <memory>

int main()
{
    using std::cout;
    using std::endl;
    using std::thread;
    using std::chrono::milliseconds;

    cout << "Exceptions-training: exception from std::thread passed to main" << endl;

    // Installing custom terminate handler to confirm termination
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    try
    {
        std::promise<void> myPromise;
        std::future<void> myFuture = myPromise.get_future();

        std::thread worker([&myPromise](){
            try
            {
                cout << "Thread routine started" << endl;
                std::this_thread::sleep_for(milliseconds(400));

                throw std::runtime_error("Exception from worker");
            }
            catch (...)
            {
                myPromise.set_exception(std::current_exception());
            }
        });

        // RAII used to join worker when going out of scope
        std::unique_ptr<std::thread, std::function<void(std::thread*)>> threadJoiner(
            &worker, [](std::thread* thread) { if (thread->joinable()) thread->join(); }
        );

        cout << "Waiting for future..." << endl;
        myFuture.wait();    // Waits until thread execution finish
        myFuture.get();     // Throws an exception that was caught within thread
    }
    catch (const std::exception& error)
    {
        cout << "Handling error: " << error.what() << endl;
    }
    catch (...)
    {
        cout << "Handling any error" << endl;
    }
    
    return EXIT_SUCCESS;
}
