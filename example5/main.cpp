#include <iostream>
#include <string>
#include <exception>

// Connection class is throwing an exception from destructor
class Connection
{
public:
    Connection()
    {
        std::cout << "Connection ctor" << std::endl;
    }

    // Throwing constructor - marked explicitly as noexcept(false)
    // to prevent terminating the application always when dtor is used
    ~Connection() noexcept(false)
    {
        std::cout << "Connection dtor" << std::endl;

        // Throwing an exception from dtor
        throw std::runtime_error("runtime");
    }
};

int main()
{
    using std::cout;
    using std::endl;

    cout << "Exceptions-training: prevent destructors from throwing" << endl;

    // Installed terminate handler should be executed when an exception
    // from destructor is thrown during stack unwinding
    std::set_terminate([](){
        cout << "Custom terminate-handler called" << endl;
        std::abort();
    });

    try
    {
        // Connection dtor should be called during stack-unwinding
        Connection connection;

        // Simulating an exception - catch handlers won't be activated,
        // as the application would be terminated earlier
        throw 1;
    }
    catch (std::exception& error)
    {
        // Handling standard exceptions
        cout << "Standard exception caught: " << error.what() << endl;
    }
    catch (...)
    {
        // Handling anything
        cout << "Non-standard exception caught" << endl;
    }
    
    return EXIT_SUCCESS;
}
