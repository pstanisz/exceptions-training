#include <cstdlib>

// This example demonstrates the difference between
// generated assembly code for ordinary function,
// function marked as throw() and noexcept
// Uncomment the variant to check and look at assembly

void foo()
{
    int* ptr = new int;
    delete ptr;
}

// void foo() throw()
// {
//     int* ptr = new int;
//     delete ptr;
// }

// void foo() noexcept
// {
//     int* ptr = new int;
//     delete ptr;
// }

int main()
try
{
    foo();

    return EXIT_SUCCESS;
}
catch (...)
{
    return EXIT_FAILURE;
}
